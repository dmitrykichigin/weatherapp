//
//  ForecastDetailView.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class TitleValueView: UIView {
    
    private(set) var titleLabel: UILabel!
    private(set) var valueLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        addSubview(titleLabel)
        
        valueLabel = UILabel()
        addSubview(valueLabel)
        
        makeConstraints()
    }
    
    func config(with title: String, value: String) {
        titleLabel.text = title
        valueLabel.text = value
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private methods
    
    private func makeConstraints() {
        titleLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        valueLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate(
            [titleLabel.topAnchor.constraint(equalTo: topAnchor),
             titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
             titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
             
             valueLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
             valueLabel.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 5),
             valueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -5)
            ])
    }
}
