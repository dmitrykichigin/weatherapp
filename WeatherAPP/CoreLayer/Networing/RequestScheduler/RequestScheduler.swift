//
//  RequestScheduler.swift
//  WeatherAPP
//
//  Created by Dmitriy Kichigin on 04/23/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Alamofire

protocol RequestScheduler {
    
    func execute <T: Decodable> (builder: RequestBuilder, decoder: JSONDecoder, completion: @escaping (_ result: Result<T>) -> Void)
}

extension RequestScheduler {
    
    func execute <T: Decodable> (builder: RequestBuilder, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (_ result: Result<T>) -> Void) {
        execute(builder: builder, decoder: decoder, completion: completion)
    }
}
