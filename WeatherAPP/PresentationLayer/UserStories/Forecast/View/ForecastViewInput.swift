//
//  ForecastViewInput.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

protocol ForecastViewInput: class {
    
    func showAlert(with message: String)
    func updateView(with list: [Forecast], for city: String?)
    func showLoading()
    func hideLoading()
}

extension ForecastViewInput {
    
    func updateView(with list: [Forecast], for city: String? = nil) {
        updateView(with: list, for: city)
    }
}
