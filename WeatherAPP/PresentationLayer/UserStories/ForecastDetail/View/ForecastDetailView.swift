//
//  ForecastDetailView.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastDetailView: UIView {
    
    private(set) var scrollView: UIScrollView!
    private(set) var stackView: StackView!
    
    private(set) var mainBlock: MainBlock!
    private(set) var weatherBlock: WeatherBlock!
    private(set) var cloudBlock: CloudBlock!
    private(set) var windBlock: WindBlock!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollView = UIScrollView()
        scrollView.backgroundColor = .lightGray
        addSubview(scrollView)
        
        stackView = StackView()
        stackView.distribution = .equalSpacing
        stackView.layoutMargins = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        stackView.isLayoutMarginsRelativeArrangement = true
        scrollView.addSubview(stackView)
        
        mainBlock = MainBlock()
        mainBlock.backgroundColor = .white
        stackView.addArrangedSubview(mainBlock)
        
        weatherBlock = WeatherBlock()
        weatherBlock.backgroundColor = .white
        stackView.addArrangedSubview(weatherBlock)
        
        cloudBlock = CloudBlock()
        cloudBlock.backgroundColor = .white
        stackView.addArrangedSubview(cloudBlock)
        
        windBlock = WindBlock()
        windBlock.backgroundColor = .white
        stackView.addArrangedSubview(windBlock)
        
        makeConstraints()
    }
    
    func config(with forecast: Forecast) {
        mainBlock.config(with: forecast.main)
        if let weatherPart = forecast.weather.first {
            weatherBlock.config(with: weatherPart)
        }
        cloudBlock.config(with: forecast.clouds)
        windBlock.config(with: forecast.wind)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Private methods

    private func makeConstraints() {
        scrollView.addStretchingConstraints(equalTo: self)
        stackView.addStretchingConstraints(equalTo: scrollView)
        scrollView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
    }
}
