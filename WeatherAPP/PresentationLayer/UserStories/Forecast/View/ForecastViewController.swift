//
//  ForecastViewController.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController {

    private var _view: ForecastView {
        return view as! ForecastView
    }

    var output: ForecastViewOutput!
    
    private var forecastList: [Forecast] = []
    
    override func loadView() {
        self.view = ForecastView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(setupSearchBar))
        title = Localization.General.appName
        
        setupTableView()
        
        output.viewDidLoad()
    }
    
    @objc func setupSearchBar() {
        navigationItem.rightBarButtonItem = nil
        
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.becomeFirstResponder()
        navigationItem.titleView = searchBar
    }
    
    private func setupTitleBar(with city: String?) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(setupSearchBar))
        navigationItem.titleView = nil
    }
    
    private func setupTableView() {
        _view.tableView.delegate = self
        _view.tableView.dataSource = self
        _view.tableView.register(ForecastCell.self, forCellReuseIdentifier: String(describing: ForecastCell.self))
    }
}

// MARK: - ForecastViewInput

extension ForecastViewController: ForecastViewInput {
    
    func showAlert(with message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Localization.General.ok, style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    func updateView(with list: [Forecast], for city: String?) {
        title = city ?? Localization.General.appName
        forecastList = list
        _view.tableView.reloadData()
    }
    
    func showLoading() {
        _view.progressView.startAnimating()
    }
    
    func hideLoading() {
        _view.progressView.stopAnimating()
    }
}

// MARK: - TableDelegate

extension ForecastViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedForecast = forecastList[indexPath.row]
        output?.didSelect(selectedForecast)
    }
}

// MARK: - TableDataSource

extension ForecastViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: ForecastCell.self), for: indexPath)
        
        (cell as? ForecastCell)?.config(with: forecastList[indexPath.row])
        
        return cell
    }
}

// MARK: - UISearchBarDelegate

extension ForecastViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        if let searchText = searchBar.text {
            output.searchEnd(with: searchText)
            setupTitleBar(with: searchText)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        setupTitleBar(with: nil)
    }
}
