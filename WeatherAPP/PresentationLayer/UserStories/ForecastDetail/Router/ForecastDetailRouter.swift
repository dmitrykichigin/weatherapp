//
//  ForecastDetailRouter.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastDetailRouter {

    weak var transitionHandler: ViperModuleTransitionHandler?
}

// MARK: - ForecastDetailRouterInput

extension ForecastDetailRouter: ForecastDetailRouterInput {
    
}