//
//  ForecastDetailPresenter.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastDetailPresenter {

    weak var moduleOutput: ForecastDetailModuleOutput?
    weak var view: ForecastDetailViewInput?
    var interactor: ForecastDetailInteractorInput!
    var router: ForecastDetailRouterInput!
    
    private var forecast: Forecast?
}

// MARK: - ForecastDetailModuleInput

extension ForecastDetailPresenter: ForecastDetailModuleInput {
    
    func config(with forecast: Forecast) {
        self.forecast = forecast
    }
}

// MARK: - ForecastDetailViewOutput

extension ForecastDetailPresenter: ForecastDetailViewOutput {

    func viewDidLoad() {
        if let forecast = forecast {
            view?.config(with: forecast)
        }
    }
}
