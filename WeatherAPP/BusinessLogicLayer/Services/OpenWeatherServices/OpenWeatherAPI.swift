//
//  OpenWeatherAPI.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 23/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

enum OpenWeatherAPI {
    case fiveDaysForecast(city: String)
    
    func builder() -> RequestBuilder {
        var builder: RequestBuilder
        
        switch self {
        case .fiveDaysForecast(let city):
            let params = ["q": city,
                          "APPID": "aabc83cff89b140a4b41089824c395bb",
                          "units": "metric"]
            builder = RestRequestBuilder(path: "/data/2.5/forecast", method: .get, params: params)
        }
        
        builder.baseURLString = ApplicationAssembly.serviceURL
        return builder
    }
}
