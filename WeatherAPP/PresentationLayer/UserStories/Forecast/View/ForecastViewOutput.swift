//
//  ForecastViewOutput.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

protocol ForecastViewOutput {
    
    func viewDidLoad()
    func searchEnd(with text: String)
    func didSelect(_ forecast: Forecast)
}

