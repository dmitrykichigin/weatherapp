//
//  AlamofireRequestScheduler.swift
//  Tayphoon
//
//  Created by Dmitry Kichigin on 23/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireRequestScheduler: RequestScheduler {
    private let sessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        return Session(configuration: configuration)
    }()
    
    func execute<T: Decodable>(builder: RequestBuilder, decoder: JSONDecoder, completion: @escaping (_ result: Result<T>) -> Void) {
        
        sessionManager.request(builder).responseDecodable (decoder: decoder) { (response: DataResponse<T>) in
            switch response.result {
            case .success:
                completion(response.result)
                
            case .failure(let error):
                if let statusCode = response.response?.statusCode, let requestError = RequestError(rawValue: statusCode) {
                    completion(.failure(requestError))
                }
                else {
                    completion(.failure(error))
                }
            }
        }
    }
}
