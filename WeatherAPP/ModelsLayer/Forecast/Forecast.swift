//
//  Forecast.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

struct Forecast: Codable {
    let dateStamp: Double
    let main: MainPart
    let weather: [WeatherPart]
    let clouds: CloudsPart
    let wind: WindPart
    
    enum CodingKeys: String, CodingKey {
        case dateStamp = "dt"
        case main
        case weather
        case clouds
        case wind
    }
}
