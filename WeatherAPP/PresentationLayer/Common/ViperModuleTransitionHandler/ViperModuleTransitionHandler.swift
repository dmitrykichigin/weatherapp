//
//  ViperModuleTransitionHandler.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

protocol ViperModuleTransitionHandler: class {
    
    /**
     Module transitions
     */
    
    func openModule(controller: UIViewController, animated: Bool)
    
    func presentModule(controller: UIViewController, animated: Bool)
    
    func closeCurrentModule(animated: Bool)
    
    func closeOpenedModules(animated: Bool)
    
    func navigateToRootModule(animated: Bool)
}
