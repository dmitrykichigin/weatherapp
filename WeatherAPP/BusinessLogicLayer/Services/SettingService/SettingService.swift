//
//  SettingService.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 16/05/2019.
//

import Foundation

protocol SettingService {
    
    func saveLastSearch(city: String)
    func loadLastSearchCity() -> String?
}
