//
//  ForecastPresenter.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastPresenter {

    weak var moduleOutput: ForecastModuleOutput?
    weak var view: ForecastViewInput?
    var interactor: ForecastInteractorInput!
    var router: ForecastRouterInput!
    
    var lastSearchCity: String?
    
    private func getForecast(for city: String) {
        view?.showLoading()
        view?.updateView(with: [])
        interactor.getForecast(city: city, completion: getForecastHandler)
    }
    
    private func getForecastHandler(_ forecastList: [Forecast], _ error: Error?) {
        view?.hideLoading()
        if let error = error {
            errorHandling(error: error)
        }
        else {
            saveCity()
            view?.updateView(with: forecastList, for: lastSearchCity)
        }
    }
    
    private func errorHandling(error: Error) {
        if let requestError = error as? RequestError, requestError == .notFound {
            view?.showAlert(with: Localization.Forecast.ErrorAlert.notFound)
        }
        else {
            view?.showAlert(with: Localization.Forecast.ErrorAlert.unknown)
        }
    }
    
    private func saveCity() {
        if let city = lastSearchCity {
            interactor.saveSearch(city: city)
        }
    }
}

// MARK: - ForecastModuleInput

extension ForecastPresenter: ForecastModuleInput {

}

// MARK: - ForecastViewOutput

extension ForecastPresenter: ForecastViewOutput {

    func viewDidLoad() {
        if let city = interactor?.getLastCity() {
            lastSearchCity = city
            getForecast(for: city)
        }
    }
    
    func searchEnd(with text: String) {
        lastSearchCity = text
        getForecast(for: text)
    }
    
    func didSelect(_ forecast: Forecast) {
        router.openDetail(with: forecast)
    }
}
