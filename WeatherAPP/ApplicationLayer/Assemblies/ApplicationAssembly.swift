//
//  ApplicationAssembly.swift
//  WeatherAPP
//
//  Created by Dmitriy Kichigin on 04/23/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

class ApplicationAssembly {
    
    static var appRouter: AppRouter = {
        return MainAppRouter()
    }()
    
    static var environment: Environment = {
        let configuration = Bundle.main.object(forInfoDictionaryKey: "Environment") as! String
        let environment = Environment(rawValue: configuration)!
        return environment
    }()
    
    static var serviceURL: String = {
        return environment.info(in: "OpenWeatherAPI", at: "URLs")!
    }()
    
    static var requestScheduler: RequestScheduler = {
        var requestScheduler = AlamofireRequestScheduler()
        return requestScheduler
    }()
}
