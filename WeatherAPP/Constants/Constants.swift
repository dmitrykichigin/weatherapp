//
//  Constants.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 14/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

struct Constants {
    
    struct URLs {
        
        static let imageURL = "https://openweathermap.org/img/w/%@.png"
    }
    
    struct UserDefaultKeys {
        
        static let lastSearchCity = "LastSearchCity"
    }
}
