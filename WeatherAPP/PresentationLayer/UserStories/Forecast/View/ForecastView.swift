//
//  ForecastView.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastView: UIView {
    
    private(set) var tableView: UITableView!
    private(set) var progressView: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView = UITableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .lightGray
        addSubview(tableView)
        
        progressView = UIActivityIndicatorView(style: .white)
        progressView.hidesWhenStopped = true
        addSubview(progressView)
        
        makeConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Private methods

    private func makeConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.addStretchingConstraints(equalTo: self)
        
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.addStretchingConstraints(equalTo: self)
    }
}
