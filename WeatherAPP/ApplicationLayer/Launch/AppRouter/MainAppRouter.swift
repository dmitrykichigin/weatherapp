//
//  MainAppRouter.swift
//  WeatherAPP
//
//  Created by Dmitriy Kichigin on 04/23/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class MainAppRouter {
    
    private(set) var window: UIWindow!

    private func openModule(_ controller: UIViewController) {
        createWindowIfNeeded()
        window.backgroundColor = UIColor.white
        window.rootViewController = controller
        makeKeyAndVisibleIfNeeded()
    }
    
    private func createWindowIfNeeded() {
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
    }
    
    private func makeKeyAndVisibleIfNeeded() {
        if !window.isKeyWindow {
            window.makeKeyAndVisible()
        }
    }
}

extension MainAppRouter: AppRouter {
    
    func openMainModule() {
        let contoller = ForecastAssembly.createModule()
        let navigationController = UINavigationController(rootViewController: contoller)
        navigationController.navigationBar.isTranslucent = false
        openModule(navigationController)
    }
}
