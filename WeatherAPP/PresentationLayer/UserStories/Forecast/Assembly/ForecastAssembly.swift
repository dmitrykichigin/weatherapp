//
//  ForecastAssembly.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

final class ForecastAssembly {
    
    static func createModule(_ configuration: ((ForecastModuleInput) -> Void)? = nil) -> UIViewController {
        let view = ForecastViewController()
        let interactor = ForecastInteractor()
        let presenter = ForecastPresenter()
        let router = ForecastRouter()
        
        interactor.weatherService = DefaultOpenWeatherServices()
        interactor.settingService = DefaultSettingService()
        
        view.output = presenter
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        router.transitionHandler = view
        
        if let configuration = configuration {
            configuration(presenter)
        }
        
        return view
    }
}
