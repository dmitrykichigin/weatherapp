//
//  ForecastPresenterTests.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 17/05/2019.
//

import XCTest
@testable import WeatherAPP

class ForecastPresenterTests: XCTestCase {
    
    var presenter: ForecastPresenter!
    var interactor: FakeForecastInteractor!
    var router: FakeForecastRouter!
    var view: FakeForecastView!
    
    override func setUp() {
        super.setUp()
        
        presenter = ForecastPresenter()
        interactor = FakeForecastInteractor()
        router = FakeForecastRouter()
        view = FakeForecastView()
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view
    }
    
    func testGetForecast() {
        
        // Given
        let searchCity = "Moscow"
        
        // When
        presenter.searchEnd(with: searchCity)
        
        // Then
        XCTAssertTrue(interactor.isGetForecast)
        XCTAssertTrue(view.isLoading)
        XCTAssertTrue(view.isForecastListSetup)
        XCTAssertTrue(view.isForecastListEmpty)
    }
    
    func testSuccesGetForecastHandler() {
        
        // Given
        let searchCity = "Moscow"
        
        // When
        presenter.searchEnd(with: searchCity)
        interactor.completion?()
        
        // Then
        XCTAssertFalse(view.isLoading)
        XCTAssertEqual(interactor.getLastCity(), searchCity)
        XCTAssertEqual(view.city, searchCity)
        XCTAssertTrue(view.isForecastListSetup)
        XCTAssertFalse(view.isForecastListEmpty)
    }
    
    func testFailureGetForecastHandler() {
        
        // Given
        let searchCity = "Moscow"
        interactor.isComplitionWithError = true
        
        // When
        presenter.searchEnd(with: searchCity)
        interactor.completion?()
        
        // Then
        XCTAssertFalse(view.isLoading)
        XCTAssertNil(interactor.getLastCity())
        XCTAssertNil(view.city)
        XCTAssertTrue(view.isForecastListSetup)
        XCTAssertTrue(view.isForecastListEmpty)
        XCTAssertTrue(view.isAlertShow)
    }
    
   func testViewDidLoadWithSavedCity() {
    
        // Given
        let seachCity = "FakeCity"
    
        // When
        interactor.saveSearch(city: seachCity)
        presenter.viewDidLoad()
    
        // Then
        XCTAssertEqual(interactor.city, seachCity)
        XCTAssertTrue(interactor.isGetForecast)
    }
    
    func testViewDidLoadWithoutSavedCity() {
        
        // When
        presenter.viewDidLoad()
        
        // Then
        XCTAssertNil(interactor.city)
        XCTAssertFalse(interactor.isGetForecast)
    }
    
    func testSearchEnd() {
        
        // Given
        let searchCity = "Moscow"
        
        // When
        presenter.searchEnd(with: searchCity)
        
        // Then
        XCTAssertEqual(presenter.lastSearchCity, searchCity)
        XCTAssertTrue(interactor.isGetForecast)
    }
    
    func testDidSelectProject() {
        
        // Given
        let forecast = Forecast(dateStamp: 0,
                                main: MainPart(temp: 0, tempMin: 0, tempMax: 0, pressure: 0, seaLevel: 0, grndLevel: 0, humidity: 0),
                                weather: [],
                                clouds: CloudsPart(all: 0),
                                wind: WindPart(speed: 0, deg: 0))
        
        // When
        presenter.didSelect(forecast)
        
        // Then
        XCTAssertTrue(router.isDetailModuleOpen)
    }
}
