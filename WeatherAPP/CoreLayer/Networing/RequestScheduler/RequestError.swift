//
//  RequestError.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

enum RequestError: Int, Error {
    case notFound = 404
    case internalServerError = 500
}
