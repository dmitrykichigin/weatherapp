//
//  StackView.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 15/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class StackView: UIStackView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.axis = .vertical
        self.alignment = .fill
        self.spacing = 5
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
