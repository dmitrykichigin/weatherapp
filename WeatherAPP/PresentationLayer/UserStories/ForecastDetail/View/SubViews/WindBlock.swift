//
//  MainBlock.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class WindBlock: UIView {
    
    private(set) var stackView: StackView!
    
    private(set) var speedView: TitleValueView!
    private(set) var directionView: TitleValueView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 8.0
        
        stackView = StackView()
        addSubview(stackView)
        
        speedView = TitleValueView()
        stackView.addArrangedSubview(speedView)
        
        directionView = TitleValueView()
        stackView.addArrangedSubview(directionView)
        
        makeConstraints()
    }
    
    func config(with windPart: WindPart) {
        speedView.config(with: Localization.General.Unit.windSpeed, value: "\(windPart.speed) m/c")
        directionView.config(with: Localization.General.Unit.windDirection, value: "\(windPart.deg)")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private methods
    
    private func makeConstraints() {
        stackView.addStretchingConstraints(equalTo: self)
    }
}
