//
//  StorageProvider.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 16/05/2019.
//

import Foundation

protocol StorageProvider {
    
    func set(_ value: Any?, forKey defaultName: String)
    
    func string(forKey defaultName: String) -> String?
}
