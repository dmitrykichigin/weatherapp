//
//  DateFormatter.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

class DateFormatterFactory {
    
    enum DateFormats: String {
        case forecastDate = "dd MMM HH:mm"
    }
    
    private static var formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        return dateFormatter
    }()
    
    static func makeString(from date: Date, format: DateFormats) -> String {
        formatter.dateFormat = format.rawValue
        return formatter.string(from: date)
    }
}
