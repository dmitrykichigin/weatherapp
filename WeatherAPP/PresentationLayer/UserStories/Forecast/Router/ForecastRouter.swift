//
//  ForecastRouter.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastRouter {
    
    weak var transitionHandler: ViperModuleTransitionHandler?
}

// MARK: - ForecastRouterInput

extension ForecastRouter: ForecastRouterInput {
    
    func openDetail(with forecast: Forecast) {
        let controller = ForecastDetailAssembly.createModule { (input) in
            input.config(with: forecast)
        }
        
        transitionHandler?.openModule(controller: controller, animated: true)
    }
}
