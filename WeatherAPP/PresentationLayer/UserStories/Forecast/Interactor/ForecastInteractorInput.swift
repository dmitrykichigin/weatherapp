//
//  ForecastInteractorInput.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

protocol ForecastInteractorInput {
    
    func getForecast(city: String, completion: @escaping (_ forecastList: [Forecast], _ error: Error?) -> Void)
    func saveSearch(city: String)
    func getLastCity() -> String?
}
