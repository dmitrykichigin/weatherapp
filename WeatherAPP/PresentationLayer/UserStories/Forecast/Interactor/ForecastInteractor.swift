//
//  ForecastInteractor.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

class ForecastInteractor {
    
    var weatherService: OpenWeatherServices?
    var settingService: SettingService?
}

// MARK: - ForecastInteractorInput

extension ForecastInteractor: ForecastInteractorInput {
    
    func getForecast(city: String, completion: @escaping (_ forecastList: [Forecast], _ error: Error?) -> Void) {
        weatherService?.getFiveDaysForecast(city: city, completion: completion)
    }
    
    func saveSearch(city: String) {
        settingService?.saveLastSearch(city: city)
    }
    
    func getLastCity() -> String? {
        return settingService?.loadLastSearchCity()
    }
}
