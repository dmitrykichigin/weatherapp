//
//  FakeForecastInteractor.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 17/05/2019.
//

import Foundation
@testable import WeatherAPP

class FakeForecastInteractor {
    private(set) var city: String?
    private(set) var isGetForecast = false
    private(set) var completion: (() -> Void)?
    
    var isComplitionWithError = false
    
    private func setup(completion: @escaping ([Forecast], Error?) -> Void) {
        let forecast = Forecast(dateStamp: 0,
                                main: MainPart(temp: 0, tempMin: 0, tempMax: 0, pressure: 0, seaLevel: 0, grndLevel: 0, humidity: 0),
                                weather: [],
                                clouds: CloudsPart(all: 0),
                                wind: WindPart(speed: 0, deg: 0))
        
        if isComplitionWithError {
            self.completion = {
                completion([], RequestError.internalServerError)
            }
        }
        else {
            self.completion = {
                completion([forecast], nil)
            }
        }
    }
}

extension FakeForecastInteractor: ForecastInteractorInput {
    
    func getForecast(city: String, completion: @escaping ([Forecast], Error?) -> Void) {
        isGetForecast = true
        setup(completion: completion)
    }
    
    func saveSearch(city: String) {
        self.city = city
    }
    
    func getLastCity() -> String? {
        return self.city
    }
}
