//
//  ForecastDetailViewController.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastDetailViewController: UIViewController {

    private var _view: ForecastDetailView {
        return view as! ForecastDetailView
    }

    var output: ForecastDetailViewOutput!

    override func loadView() {
        self.view = ForecastDetailView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        output.viewDidLoad()
    }
}

// MARK: - ForecastDetailViewInput

extension ForecastDetailViewController: ForecastDetailViewInput {

    func config(with forecast: Forecast) {
        let date = Date(timeIntervalSince1970: forecast.dateStamp)
        title = DateFormatterFactory.makeString(from: date, format: .forecastDate)
        _view.config(with: forecast)
    }
}
