//
//  Color+Palette.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let lightGray = UIColor(red: 0.83, green: 0.83, blue: 0.83, alpha: 1)
}
