//
//  ForecastDetailAssembly.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

final class ForecastDetailAssembly {
    
    static func createModule(_ configuration: ((ForecastDetailModuleInput) -> Void)? = nil) -> UIViewController {
        let view = ForecastDetailViewController()
        let interactor = ForecastDetailInteractor()
        let presenter = ForecastDetailPresenter()
        let router = ForecastDetailRouter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        router.transitionHandler = view
        
        if let configuration = configuration {
            configuration(presenter)
        }
        
        return view
    }
}
