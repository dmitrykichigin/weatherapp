//
//  OpenWeatherServiceTests.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 16/05/2019.
//

import XCTest
import OHHTTPStubs
@testable import WeatherAPP

class OpenWeatherServiceTests: XCTestCase {

    func testGetForecast() {
        
        // Given
        let service = DefaultOpenWeatherServices()
        let urlHost = URL(string: ApplicationAssembly.serviceURL)?.host
        
        let expectation = self.expectation(description: "TestGetForecast")
        
        stub(condition: isHost(urlHost!) && isPath("/data/2.5/forecast")) { _ in
            let path = OHPathForFile("forecast.json", type(of: self))
            return OHHTTPStubsResponse(fileAtPath: path!,
                                       statusCode: 200,
                                       headers: .none)
        }
        
        // When
        service.getFiveDaysForecast(city: "Test") { (forecastList, error) in
            
            // Then
            XCTAssertNil(error)
            XCTAssertEqual(forecastList.count, 2)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: .none)
        
        // tearDown
        OHHTTPStubs.removeAllStubs()
    }
    
    func testGetForecastWithNotFound() {
        
        // Given
        let service = DefaultOpenWeatherServices()
        let urlHost = URL(string: ApplicationAssembly.serviceURL)?.host
        
        let expectation = self.expectation(description: "TestGetForecastWithNotFound")
        
        stub(condition: isHost(urlHost!) && isPath("/data/2.5/forecast")) { _ in
            return OHHTTPStubsResponse(jsonObject: [:],
                                       statusCode: 404,
                                       headers: nil)
        }
        
        // When
        service.getFiveDaysForecast(city: "Test") { (forecastList, error) in
            
            // Then
            XCTAssertEqual((error as? RequestError), RequestError.notFound)
            XCTAssertEqual(forecastList.count, 0)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: .none)
        
        // tearDown
        OHHTTPStubs.removeAllStubs()
    }
    
    func testGetForecastWithUnknowError() {
        
        // Given
        let service = DefaultOpenWeatherServices()
        let urlHost = URL(string: ApplicationAssembly.serviceURL)?.host
        
        let expectation = self.expectation(description: "TestGetForecastWithUnknowError")
        
        stub(condition: isHost(urlHost!) && isPath("/data/2.5/forecast")) { _ in
            return OHHTTPStubsResponse(jsonObject: [:],
                                       statusCode: 500,
                                       headers: nil)
        }
        
        // When
        service.getFiveDaysForecast(city: "Test") { (forecastList, error) in
            
            // Then
            XCTAssertEqual((error as? RequestError), RequestError.internalServerError)
            XCTAssertEqual(forecastList.count, 0)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: .none)
        
        // tearDown
        OHHTTPStubs.removeAllStubs()
    }
}
