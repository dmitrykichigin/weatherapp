//
//  BaseOpenWeatherServices.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 23/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation
import Alamofire

fileprivate struct Response: Codable {
    
    let list: [Forecast]
}

class DefaultOpenWeatherServices: OpenWeatherServices {
    
    var requestScheduler: RequestScheduler
    
    init(requestScheduler: RequestScheduler = ApplicationAssembly.requestScheduler) {
        self.requestScheduler = requestScheduler
    }

    func getFiveDaysForecast(city: String, completion: @escaping (_ forecastList: [Forecast], _ error: Error?) -> Void) {
        let builder = OpenWeatherAPI.fiveDaysForecast(city: city).builder()
        
        requestScheduler.execute(builder: builder) { (result: Result<Response?>) in
            switch result {
            case .success(let result):
                completion(result?.list ?? [], nil)
                
            case .failure(let error):
                completion([], error)
            }
        }
    }
}
