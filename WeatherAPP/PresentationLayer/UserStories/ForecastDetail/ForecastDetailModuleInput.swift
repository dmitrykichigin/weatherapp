//
//  ForecastDetailModuleInput.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

protocol ForecastDetailModuleInput: class {
    
    var moduleOutput: ForecastDetailModuleOutput? { get set }
    
    func config(with forecast: Forecast)
}
