//
//  DateFormatterFactoryTests.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 16/05/2019.
//

import XCTest
@testable import WeatherAPP

class DateFormatterFactoryTests: XCTestCase {

    func testMakeString() {
        
        // Given
        let date = Date(timeIntervalSince1970: 1558035000)
        let correctForecastDateString = "16 May 22:30"
        
        // When
        let newDateString = DateFormatterFactory.makeString(from: date, format: .forecastDate)
        
        // Then
        XCTAssertEqual(correctForecastDateString, newDateString)
    }
}
