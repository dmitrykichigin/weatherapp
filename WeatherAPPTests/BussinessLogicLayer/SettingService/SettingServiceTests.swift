//
//  SettingServiceTests.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 16/05/2019.
//

import XCTest
@testable import WeatherAPP

class SettingServiceTests: XCTestCase {

    func testWriteAndReadSettings() {
        
        // Given
        let settingService = DefaultSettingService(provider: UserDefaults(suiteName: "FakeUserDefaults")!)
        let city = "Moscow"
        
        // When
        settingService.saveLastSearch(city: city)
        let readValue = settingService.loadLastSearchCity()
        
        // Then
        XCTAssertEqual(city, readValue)
    }
}
