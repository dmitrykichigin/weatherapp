//
//  OpenWeatherServices.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 23/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

protocol OpenWeatherServices {
    
    func getFiveDaysForecast(city: String, completion: @escaping (_ forecastList: [Forecast], _ error: Error?) -> Void)
}
