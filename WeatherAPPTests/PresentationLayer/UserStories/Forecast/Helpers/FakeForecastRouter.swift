//
//  FakeForecastRouter.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 17/05/2019.
//

import Foundation
@testable import WeatherAPP

class FakeForecastRouter {
    
   private(set) var isDetailModuleOpen = false
}

extension FakeForecastRouter: ForecastRouterInput {
    
    func openDetail(with forecast: Forecast) {
        isDetailModuleOpen = true
    }
}
