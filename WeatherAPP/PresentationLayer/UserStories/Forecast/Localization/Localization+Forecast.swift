//
//  Localization+Forecast.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

extension Localization {
    
    struct Forecast {
        
        struct ErrorAlert {
            
            static var notFound: String = {
                return "forecast.error.notFound".localized()
            }()
            
            static var unknown: String = {
                return "forecast.error.unknown".localized()
            }()
        }
    }
}
