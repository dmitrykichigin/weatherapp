# WeatherAPP
=============

WeatherAPP iOS client.

## Installation

- Installing WeatherAPP dependencies via the [CocoaPods](http://cocoapods.org/) package manager, as it provides flexible dependency management and dead simple installation. For best results, it is recommended that you install via CocoaPods **>= 0.19.1** using Git **>= 1.8.0** installed via Homebrew.

### via CocoaPods

Install CocoaPods if not already available:

``` bash
$ [sudo] gem install cocoapods
$ pod setup
```

Change to the directory of your Xcode project:

``` bash
$ cd /path/to/WeatherAPPProject
```

Install WeatherAPP dependencies:

``` bash
$ pod install
```

Open WeatherAPP project in Xcode from the WeatherAPP.xcworkspace file (not the usual project file)

``` bash
$ open WeatherAPP.xcworkspace
```

- Start coding).

## License

WeatherAPP is licensed under the terms of the [Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html). Please see the [LICENSE](LICENSE) file for full details.
