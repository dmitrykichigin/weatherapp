//
//  ForecastCell.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 23/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell {
    
    private(set) var underlayView: UIView!
    
    private(set) var dateLabel: UILabel!
    private(set) var iconView: UIImageView!
    
    private(set) var tempView: TitleValueView!
    private(set) var pressureView: TitleValueView!
    private(set) var humidityView: TitleValueView!
    
    private let imageSize = CGSize(width: 30, height: 30)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .lightGray
        selectionStyle = .none
        
        underlayView = UIView()
        underlayView.backgroundColor = .white
        underlayView.layer.cornerRadius = 8.0
        contentView.addSubview(underlayView)
        
        dateLabel = UILabel()
        dateLabel.font = .boldSystemFont(ofSize: 18)
        underlayView.addSubview(dateLabel)
        
        iconView = UIImageView()
        underlayView.addSubview(iconView)
        
        tempView = TitleValueView()
        underlayView.addSubview(tempView)
        
        pressureView = TitleValueView()
        underlayView.addSubview(pressureView)
        
        humidityView = TitleValueView()
        underlayView.addSubview(humidityView)
        
        makeConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(with forecast: Forecast) {
        let date = Date(timeIntervalSince1970: forecast.dateStamp)
        dateLabel.text = DateFormatterFactory.makeString(from: date, format: .forecastDate)
        
        if let stringUrl = forecast.weather.first?.icon {
            let url = URL(string: String(format: Constants.URLs.imageURL, stringUrl))
            iconView.kf.setImage(with: url)
        }
        
        tempView.config(with: Localization.General.Unit.temp, value: "\(forecast.main.temp) C")
        pressureView.config(with: Localization.General.Unit.pressure, value: "\(forecast.main.pressure) hPa")
        humidityView.config(with: Localization.General.Unit.humidity, value: "\(forecast.main.humidity) %")
    }
    
    func makeConstraints() {
        self.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        iconView.translatesAutoresizingMaskIntoConstraints = false
        tempView.translatesAutoresizingMaskIntoConstraints = false
        pressureView.translatesAutoresizingMaskIntoConstraints = false
        humidityView.translatesAutoresizingMaskIntoConstraints = false
        
        underlayView.addStretchingConstraints(equalTo: contentView, edgeInsets: UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5))
        
        NSLayoutConstraint.activate(
            [dateLabel.leftAnchor.constraint(equalTo: underlayView.leftAnchor, constant: 5),
             dateLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
             
             iconView.rightAnchor.constraint(equalTo: underlayView.rightAnchor),
             iconView.topAnchor.constraint(equalTo: underlayView.topAnchor),
             iconView.leftAnchor.constraint(equalTo: dateLabel.rightAnchor, constant: 5),
             iconView.widthAnchor.constraint(equalToConstant: imageSize.width),
             iconView.heightAnchor.constraint(equalToConstant: imageSize.height),
             
             tempView.topAnchor.constraint(equalTo: iconView.bottomAnchor),
             tempView.leftAnchor.constraint(equalTo: underlayView.leftAnchor),
             tempView.rightAnchor.constraint(equalTo: underlayView.rightAnchor),
             
             pressureView.topAnchor.constraint(equalTo: tempView.bottomAnchor),
             pressureView.leftAnchor.constraint(equalTo: underlayView.leftAnchor),
             pressureView.rightAnchor.constraint(equalTo: underlayView.rightAnchor),
             
             humidityView.topAnchor.constraint(equalTo: pressureView.bottomAnchor),
             humidityView.leftAnchor.constraint(equalTo: underlayView.leftAnchor),
             humidityView.rightAnchor.constraint(equalTo: underlayView.rightAnchor),
             humidityView.bottomAnchor.constraint(equalTo: underlayView.bottomAnchor)])
    }
}
