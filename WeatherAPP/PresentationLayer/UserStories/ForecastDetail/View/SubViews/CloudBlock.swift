//
//  MainBlock.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class CloudBlock: UIView {
    
    private(set) var cloudsView: TitleValueView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 8.0
        
        cloudsView = TitleValueView()
        addSubview(cloudsView)
        
        makeConstraints()
    }
    
    func config(with cloudsPart: CloudsPart) {
        cloudsView.config(with: Localization.General.Unit.cloudiness, value: "\(cloudsPart.all) %")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private methods
    
    private func makeConstraints() {
        cloudsView.addStretchingConstraints(equalTo: self, edgeInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
    }
}
