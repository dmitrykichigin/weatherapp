//
//  Localization.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

struct Localization {
    
    struct General {
        
        static var ok: String = {
            return "general.ok".localized()
        }()
        
        static var appName: String = {
            return "general.app.name".localized()
        }()
        
        struct Unit {
            
            static var temp: String = {
                return "general.units.temp".localized()
            }()
            
            static var tempMin: String = {
                return "general.units.minTemp".localized()
            }()
            
            static var tempMax: String = {
                return "general.units.maxTemp".localized()
            }()
            
            static var pressure: String = {
                return "general.units.pressure".localized()
            }()
            
            static var pressureSeaLevel: String = {
                return "general.units.seaLevel".localized()
            }()
            
            static var pressureGrndLevel: String = {
                return "general.units.grndLevel".localized()
            }()
            
            static var humidity: String = {
                return "general.units.humidity".localized()
            }()
            
            static var cloudiness: String = {
                return "general.units.cloudiness".localized()
            }()
            
            static var windSpeed: String = {
                return "general.units.windSpeed".localized()
            }()
            
            static var windDirection: String = {
                return "general.units.windDirection".localized()
            }()
        }
    }
}
