//
//  ObjectRequestBuilder.swift
//  WeatherAPP
//
//  Created by Dmitriy Kichigin on 04/23/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Alamofire

struct ObjectRequestBuilder<T> : RequestBuilder where T: Encodable {
    let path: String
    let method: HTTPMethod
    let object: T
    let encoder: JSONEncoder

    var baseURLString: String = ""

    init(path: String, object: T, method: HTTPMethod = .post, encoder: JSONEncoder = JSONEncoder()) {
        self.path = path
        self.method = method
        self.object = object
        self.encoder = encoder
    }

    func asURLRequest() throws -> URLRequest {
        let url = try baseURLString.asURL()
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue

        let httpBody = try encoder.encode(object)
        request.httpBody = httpBody
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        return request
    }
}
