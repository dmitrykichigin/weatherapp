//
//  UIViewController+ViperModuleTransitionHandler.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

extension UIViewController: ViperModuleTransitionHandler {
    
    // MARK: - Module transitions
    
    func openModule(controller: UIViewController, animated: Bool) {
        navigationController?.pushViewController(controller, animated: animated)
    }
    
    func presentModule(controller: UIViewController, animated: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.present(controller, animated: animated, completion: nil)
        }
    }
    
    func closeCurrentModule(animated: Bool) {
        let isInNavigationStack = (parent is UINavigationController)
        if let navigationController = parent as? UINavigationController, navigationController.children.count > 1 {
            navigationController.popViewController(animated: animated)
        }
        else if presentingViewController != nil {
            dismiss(animated: animated, completion: nil)
        }
        else if !isInNavigationStack && view.superview != nil {
            removeFromParent()
            view.removeFromSuperview()
        }
    }
    
    func closeOpenedModules(animated: Bool) {
        navigationController?.popToViewController(self, animated: animated)
    }
    
    func navigateToRootModule(animated: Bool) {
        if let navigationController = parent as? UINavigationController, navigationController.children.count > 1 {
            navigationController.popToRootViewController(animated: animated)
        }
    }
}

