//
//  MainBlock.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit
import Kingfisher

class WeatherBlock: UIView {
    
    private(set) var mainLabel: UILabel!
    private(set) var descriptionLabel: UILabel!
    private(set) var iconView: UIImageView!
    
    private let imageSize = CGSize(width: 50, height: 50)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 8.0
        
        mainLabel = UILabel()
        addSubview(mainLabel)
        
        descriptionLabel = UILabel()
        addSubview(descriptionLabel)
        
        iconView = UIImageView()
        iconView.kf.indicatorType = .activity
        addSubview(iconView)
        
        makeConstraints()
    }
    
    func config(with weatherPart: WeatherPart) {
        mainLabel.text = "\(weatherPart.main)"
        descriptionLabel.text = "\(weatherPart.description)"
        let url = URL(string: String(format: Constants.URLs.imageURL, weatherPart.icon))
        iconView.kf.setImage(with: url)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private methods
    
    private func makeConstraints() {
        iconView.translatesAutoresizingMaskIntoConstraints = false
        mainLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate(
            [iconView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
             iconView.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
             iconView.heightAnchor.constraint(equalToConstant: imageSize.height),
             iconView.widthAnchor.constraint(equalToConstant: imageSize.width),
             
             mainLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
             mainLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
             mainLabel.rightAnchor.constraint(equalTo: iconView.leftAnchor, constant: -5),
             
             descriptionLabel.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: 5),
             descriptionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
             descriptionLabel.rightAnchor.constraint(equalTo: iconView.leftAnchor, constant: -5),
             descriptionLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5)])
    }
}
