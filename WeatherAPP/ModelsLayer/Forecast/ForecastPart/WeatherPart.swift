//
//  WeatherPart.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

struct WeatherPart: Codable {
    
    let id: Int
    let main: String
    let description: String
    let icon: String
}
