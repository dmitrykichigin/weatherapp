//
//  MainBlock.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

class MainBlock: UIView {
    
    private(set) var stackView: StackView!
    
    private(set) var tempView: TitleValueView!
    private(set) var tempMinView: TitleValueView!
    private(set) var tempMaxView: TitleValueView!
    private(set) var pressureView: TitleValueView!
    private(set) var seaLevelView: TitleValueView!
    private(set) var grndLevelView: TitleValueView!
    private(set) var humidityView: TitleValueView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 8.0
        
        stackView = StackView()
        addSubview(stackView)
        
        tempView = TitleValueView()
        stackView.addArrangedSubview(tempView)
        
        tempMinView = TitleValueView()
        stackView.addArrangedSubview(tempMinView)
        
        tempMaxView = TitleValueView()
        stackView.addArrangedSubview(tempMaxView)
        
        pressureView = TitleValueView()
        stackView.addArrangedSubview(pressureView)
        
        seaLevelView = TitleValueView()
        stackView.addArrangedSubview(seaLevelView)
        
        grndLevelView = TitleValueView()
        stackView.addArrangedSubview(grndLevelView)
        
        humidityView = TitleValueView()
        stackView.addArrangedSubview(humidityView)
        
        makeConstraints()
    }
    
    func config(with mainPart: MainPart) {
        tempView.config(with: Localization.General.Unit.temp, value: "\(mainPart.temp) C")
        tempMinView.config(with: Localization.General.Unit.tempMin, value: "\(mainPart.tempMin) C")
        tempMaxView.config(with: Localization.General.Unit.tempMax, value: "\(mainPart.tempMax) C")
        pressureView.config(with: Localization.General.Unit.pressure, value: "\(mainPart.pressure) hPa")
        seaLevelView.config(with: Localization.General.Unit.pressureSeaLevel, value: "\(mainPart.seaLevel) hPa")
        grndLevelView.config(with: Localization.General.Unit.pressureGrndLevel, value: "\(mainPart.grndLevel) hPa")
        humidityView.config(with: Localization.General.Unit.humidity, value: "\(mainPart.humidity) %")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private methods
    
    private func makeConstraints() {
        stackView.addStretchingConstraints(equalTo: self)
    }
}
