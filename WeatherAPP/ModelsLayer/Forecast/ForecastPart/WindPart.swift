//
//  WindPart.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

struct WindPart: Codable {
    
    let speed: Double
    let deg: Double
}
