//
//  FakeForecastView.swift
//  WeatherAPPTests
//
//  Created by Dmitry Kichigin on 17/05/2019.
//

import Foundation
@testable import WeatherAPP

class FakeForecastView {
    private(set) var isAlertShow = false
    private(set) var isLoading = false
    
    private(set) var isForecastListEmpty = false
    private(set) var isForecastListSetup = false
    
    private(set) var city: String?
}

extension FakeForecastView: ForecastViewInput {
    func showAlert(with message: String) {
        isAlertShow = true
    }
    
    func showLoading() {
        isLoading = true
    }
    
    func hideLoading() {
        isLoading = false
    }
    
    func updateView(with list: [Forecast], for city: String? = nil) {
        isForecastListSetup = true
        self.city = city
        isForecastListEmpty = list.isEmpty
    }
}
