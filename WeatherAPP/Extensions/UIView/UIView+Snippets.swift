//
//  UIView+Snippets.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

// MARK: - Constraints Snippets
extension UIView {
    
    func addStretchingConstraints(equalTo view: UIView) {
        addStretchingConstraints(equalTo: view, edgeInsets: .zero)
    }
    
    func addStretchingConstraints(equalTo view: UIView, edgeInsets: UIEdgeInsets) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: view.topAnchor, constant: edgeInsets.top),
            leftAnchor.constraint(equalTo: view.leftAnchor, constant: edgeInsets.left),
            rightAnchor.constraint(equalTo: view.rightAnchor, constant: -edgeInsets.right),
            bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -edgeInsets.bottom)])
    }
    
    func addCenteringConstraints(equalTo view: UIView) {
        addCenteringConstraints(equalTo: view, centerOffset: .zero)
    }
    
    func addCenteringConstraints(equalTo view: UIView, centerOffset: CGPoint) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: centerOffset.x),
             centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: centerOffset.y)])
    }
    
    func addPinnedConstraints(toTop view: UIView) {
        addPinnedConstraints(toTop: view, offsetY: 0.0)
    }
    
    func addPinnedConstraints(toTop view: UIView, offsetY: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [topAnchor.constraint(equalTo: view.topAnchor, constant: offsetY),
             leftAnchor.constraint(equalTo: view.leftAnchor),
             rightAnchor.constraint(equalTo: view.rightAnchor)])
    }
    
    func addPinnedConstraints(toTop view: UIView, edgeInsets: UIEdgeInsets) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [topAnchor.constraint(equalTo: view.topAnchor, constant: edgeInsets.top),
             leftAnchor.constraint(equalTo: view.leftAnchor, constant: edgeInsets.left),
             rightAnchor.constraint(equalTo: view.rightAnchor, constant: edgeInsets.right)])
    }
    
    func addPinnedConstraints(toBottom view: UIView) {
        addPinnedConstraints(toBottom: view, offsetY: 0.0)
    }
    
    func addPinnedConstraints(toBottom view: UIView, offsetY: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [leftAnchor.constraint(equalTo: view.leftAnchor),
             rightAnchor.constraint(equalTo: view.rightAnchor),
             bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: offsetY)])
        
    }
}
