//
//  DefaultSettingService.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 16/05/2019.
//

import Foundation

class DefaultSettingService: SettingService {
    
    let provider: StorageProvider
    
    init(provider: StorageProvider = UserDefaults.standard) {
        self.provider = provider
    }
    
    func saveLastSearch(city: String) {
        provider.set(city, forKey: Constants.UserDefaultKeys.lastSearchCity)
    }
    
    func loadLastSearchCity() -> String? {
        return provider.string(forKey: Constants.UserDefaultKeys.lastSearchCity)
    }
}
