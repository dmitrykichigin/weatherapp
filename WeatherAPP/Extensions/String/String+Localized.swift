//
//  String+Localized.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 07/05/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import UIKit

extension String {
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    /// Localize string key
    
    func localized(_ value: String) -> String {
        return String.localizedStringWithFormat(self.localized(), value)
    }
}
