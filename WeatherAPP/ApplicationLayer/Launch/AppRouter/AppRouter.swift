//
//  AppRouter.swift
//  WeatherAPP
//
//  Created by Dmitriy Kichigin on 04/23/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

protocol AppRouter: class {
    
    func openMainModule()
}
