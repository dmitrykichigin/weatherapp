//
//  Environment.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 23/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

enum Environment: String, Equatable {
    
    case Development
    case Adhoc
    case Production
    
    func info(in infoDictionaryKey: String, at valuesKey: String) -> String? {
        guard let dictionary = Bundle.main.object(forInfoDictionaryKey: infoDictionaryKey) as? [String: Any] else {
            return nil
        }
        guard let values = dictionary[valuesKey] as? [String: Any] else {
            return nil
        }
        return values[self.rawValue.capitalized] as? String
    }
}
