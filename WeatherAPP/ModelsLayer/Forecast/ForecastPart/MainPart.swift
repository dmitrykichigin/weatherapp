//
//  main.swift
//  WeatherAPP
//
//  Created by Dmitry Kichigin on 24/04/2019.
//  Copyright © 2019 MyTeam. All rights reserved.
//

import Foundation

struct MainPart: Codable {
    
    let temp: Double
    let tempMin: Double
    let tempMax: Double
    let pressure: Double
    let seaLevel: Double
    let grndLevel: Double
    let humidity: Double
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax  = "temp_max"
        case pressure
        case seaLevel  = "sea_level"
        case grndLevel  = "grnd_level"
        case humidity
    }
}
